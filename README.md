﻿SRP
===

This library provides a complete SRP implementation based on the excellent base implementation by Bouncy Castle in the Bouncy Castle C# library. (Source: https://github.com/bcgit/bc-csharp/)
As this library focusses purely on providing an SRP implementation, the other parts of the bouncycastle library have been stripped out, they are available in the original bcgit repo (see link above)

Only a few minor things have been added to the existing SRP implementation:

Client
------

* Add a CalculateClientProof method to calculate the M1 proof, this proof allows the server to verify the client indeed knows the shared secret
* Add a VerifyServerProof method to calculate the M2 proof and verify it against the proof that was sent by the server, this allows the client to verify the server indeed knows the shared secret
* Add a GetKey value that returns a Hashed version of S, as dictated by the SRP spec, to be used to encrypt further communication

Server
------
* Add a VerifyClientProof method to calculate the M1 proof and verify it against the proof that was sent by the client, this allows the server to verify the client indeed knows the shared secret
* Add a CalculateServerProof method to calculate the M2 proof, this proof allows the client to verify the server indeed knows the shared secret
* Add a GetKey value that returns a Hashed version of S, as dictated by the SRP spec, to be used to encrypt further communication

License
-------

The original Bouncycastle library is licensed under an MIT like license.
As such, we've decided to license this library under MIT as well, with both Palm Stone Games and Bouncy castle as copyright holders.