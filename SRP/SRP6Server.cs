using System;

namespace SRP
{
    /// <summary>
    /// Implements the server side SRP-6a protocol. Note that this class is stateful, and therefore NOT threadsafe.
    /// This implementation of SRP is based on the optimized message sequence put forth by Thomas Wu in the paper
    /// "SRP-6: Improvements and Refinements to the Secure Remote Password Protocol, 2002"
    /// </summary>
    public class Srp6Server
    {
        protected BigInteger N;
        protected BigInteger g;
        protected BigInteger v;

        protected SecureRandom random;
        protected IDigest digest;

        protected BigInteger A;

        protected BigInteger privB;
        protected BigInteger pubB;

        protected BigInteger M1;

        protected BigInteger u;
        protected BigInteger S;

        public Srp6Server()
        {
        }

        /// <summary>
        /// Initialises the server to accept a new client authentication attempt
        /// </summary>
        /// <param name="N">The safe prime associated with the client's verifier</param>
        /// <param name="g">The group parameter associated with the client's verifier</param>
        /// <param name="v">The client's verifier</param>
        /// <param name="digest">The digest algorithm associated with the client's verifier</param>
        /// <param name="random">For key generation</param>
        public void Init(BigInteger N, BigInteger g, BigInteger v, IDigest digest, SecureRandom random)
        {
            this.N = N;
            this.g = g;
            this.v = v;

            this.random = random;
            this.digest = digest;
        }

        /// <summary>
        /// Generates the server's credentials that are to be sent to the client.
        /// </summary>
        /// <returns>The server's public value to the client</returns>
        public BigInteger GenerateServerCredentials()
        {
            BigInteger k = Srp6Utilities.CalculateK(digest, N, g);
            this.privB = SelectPrivateValue();
            this.pubB = k.Multiply(v).Mod(N).Add(g.ModPow(privB, N)).Mod(N);

            return pubB;
        }

        /// <summary>
        /// Processes the client's credentials. If valid the shared secret is generated and returned.
        /// </summary>
        /// <param name="clientA">The client's credentials</param>
        /// <returns>A shared secret BigInteger</returns>
        public BigInteger CalculateSecret(BigInteger clientA)
        {
            this.A = Srp6Utilities.ValidatePublicValue(N, clientA);
            this.u = Srp6Utilities.CalculateU(digest, N, A, pubB);
            this.S = CalculateS();

            return S;
        }

        /// <summary>
        /// Calculate the client proof M1 and return it
        /// </summary>
        /// <returns>The M1 proof the client is expected to send</returns>
        /// <remarks>This method is usually not needed to be called directly</remarks>
        public BigInteger CalculateClientProof()
        {
            M1 = Srp6Utilities.CalculateM1(digest, A, pubB, S);
            return M1;
        }

        /// <summary>
        /// Verify the proof of the secret from the client
        /// </summary>
        /// <param name="M1">The proof of knowledge of the secret: M1</param>
        /// <returns>True if the proof is correct, false otherwise</returns>
        public bool VerifyClientProof(BigInteger M1)
        {
            return M1.Equals(CalculateClientProof());
        }

        /// <summary>
        /// Calculate the server's proof of knowledge of the secret
        /// </summary>
        /// <returns>Proof of secret: M2</returns>
        /// <remarks>
        /// The client's proof must have been verified before the server's proof can be calculated
        /// </remarks>
        public BigInteger CalculateServerProof()
        {
            return Srp6Utilities.CalculateM2(digest, A, M1, S);
        }

        /// <summary>
        /// Get the hashed secret key to use to encrypt further communication
        /// </summary>
        /// <returns>Hashed Server Key K (=H(S))</returns>
        public byte[] GetKey()
        {
            return Srp6Utilities.HashedKey(digest, S);
        }


	    protected BigInteger SelectPrivateValue()
	    {
	    	return Srp6Utilities.GeneratePrivateValue(digest, N, g, random);    	
	    }

		private BigInteger CalculateS()
	    {
			return v.ModPow(u, N).Multiply(A).Mod(N).ModPow(privB, N);
	    }
	}
}
