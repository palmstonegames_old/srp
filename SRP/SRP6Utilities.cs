using System;
using SRP.Util;

namespace SRP
{
	public static class Srp6Utilities
	{
		public static BigInteger CalculateK(IDigest digest, BigInteger N, BigInteger g)
		{
			return HashPaddedPair(digest, N, N, g);
		}

	    public static BigInteger CalculateU(IDigest digest, BigInteger N, BigInteger A, BigInteger B)
	    {
	    	return HashPaddedPair(digest, N, A, B);
	    }

		public static BigInteger CalculateX(IDigest digest, BigInteger N, byte[] salt, byte[] identity, byte[] password)
	    {
	        byte[] output = new byte[digest.GetDigestSize()];

	        digest.BlockUpdate(identity, 0, identity.Length);
	        digest.Update((byte)':');
	        digest.BlockUpdate(password, 0, password.Length);
	        digest.DoFinal(output, 0);

	        digest.BlockUpdate(salt, 0, salt.Length);
	        digest.BlockUpdate(output, 0, output.Length);
	        digest.DoFinal(output, 0);

	        return new BigInteger(1, output).Mod(N);
	    }

		public static BigInteger GeneratePrivateValue(IDigest digest, BigInteger N, BigInteger g, SecureRandom random)
	    {
			int minBits = System.Math.Min(256, N.BitLength / 2);
	        BigInteger min = BigInteger.One.ShiftLeft(minBits - 1);
	        BigInteger max = N.Subtract(BigInteger.One);

	        return BigIntegers.CreateRandomInRange(min, max, random);
	    }

		public static BigInteger ValidatePublicValue(BigInteger N, BigInteger val)
		{
		    val = val.Mod(N);

	        // Check that val % N != 0
	        if (val.Equals(BigInteger.Zero))
	            throw new CryptoException("Invalid public value: 0");

		    return val;
		}

        public static BigInteger CalculateM1(IDigest digest, BigInteger A, BigInteger B, BigInteger S)
        {
            byte[] output = new byte[digest.GetDigestSize()];
            byte[] A_bytes = A.ToByteArray();
            byte[] B_bytes = B.ToByteArray();
            byte[] S_bytes = S.ToByteArray();

            digest.BlockUpdate(A_bytes, 0, A_bytes.Length);
            digest.BlockUpdate(B_bytes, 0, B_bytes.Length);
            digest.BlockUpdate(S_bytes, 0, S_bytes.Length);
            digest.DoFinal(output, 0);

            return new BigInteger(1, output);
        }

        public static BigInteger CalculateM2(IDigest digest, BigInteger A, BigInteger M1, BigInteger S)
        {
            byte[] output = new byte[digest.GetDigestSize()];
            byte[] A_bytes = A.ToByteArray();
            byte[] M1_bytes = M1.ToByteArray();
            byte[] S_bytes = S.ToByteArray();

            digest.BlockUpdate(A_bytes, 0, A_bytes.Length);
            digest.BlockUpdate(M1_bytes, 0, M1_bytes.Length);
            digest.BlockUpdate(S_bytes, 0, S_bytes.Length);
            digest.DoFinal(output, 0);

            return new BigInteger(1, output);
        }

        public static byte[] HashedKey(IDigest digest, BigInteger S)
        {
            byte[] output = new byte[digest.GetDigestSize()];

            byte[] S_bytes = S.ToByteArray();

            digest.BlockUpdate(S_bytes, 0, S_bytes.Length);
            digest.DoFinal(output, 0);

            return output;
        }

		private static BigInteger HashPaddedPair(IDigest digest, BigInteger N, BigInteger n1, BigInteger n2)
		{
	    	int padLength = (N.BitLength + 7) / 8;

	    	byte[] n1_bytes = GetPadded(n1, padLength);
	    	byte[] n2_bytes = GetPadded(n2, padLength);

	        digest.BlockUpdate(n1_bytes, 0, n1_bytes.Length);
	        digest.BlockUpdate(n2_bytes, 0, n2_bytes.Length);

	        byte[] output = new byte[digest.GetDigestSize()];
	        digest.DoFinal(output, 0);

	        return new BigInteger(1, output).Mod(N);
		}

		private static byte[] GetPadded(BigInteger n, int length)
		{
			byte[] bs = BigIntegers.AsUnsignedByteArray(n);
			if (bs.Length < length)
			{
				byte[] tmp = new byte[length];
				Array.Copy(bs, 0, tmp, length - bs.Length, bs.Length);
				bs = tmp;
			}
			return bs;
		}
	}
}
