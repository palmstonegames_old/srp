using System;

namespace SRP
{
    /// <summary>
    /// Implements the client side SRP-6a protocol. Note that this class is stateful, and therefore NOT threadsafe.
    /// This implementation of SRP is based on the optimized message sequence put forth by Thomas Wu in the paper
    /// "SRP-6: Improvements and Refinements to the Secure Remote Password Protocol, 2002"
    /// </summary>
	public class Srp6Client
	{
	    protected BigInteger N;
	    protected BigInteger g;

	    protected BigInteger privA;
	    protected BigInteger pubA;

	    protected BigInteger B;

	    protected BigInteger x;
	    protected BigInteger u;
	    protected BigInteger S;

        protected BigInteger M1;

	    protected IDigest digest;
	    protected SecureRandom random;

	    public Srp6Client()
	    {
	    }

        /// <summary>
        /// Initialises the client to begin new authentication attempt
        /// </summary>
        /// <param name="N">The safe prime associated with the client's verifier</param>
        /// <param name="g">The group parameter associated with the client's verifier</param>
        /// <param name="digest">The digest algorithm associated with the client's verifier</param>
        /// <param name="random">For key generation</param>
        /// <returns>Public Ephemereal value A</returns>
	    public BigInteger Init(BigInteger N, BigInteger g, IDigest digest, SecureRandom random)
	    {
	        this.N = N;
	        this.g = g;
	        this.digest = digest;
            this.random = random;

            this.privA = SelectPrivateValue();
            this.pubA = g.ModPow(privA, N);

            return this.pubA;
	    }

        /// <summary>
        /// Generates client's credentials given the client's salt, identity and password
        /// </summary>
        /// <param name="salt">The salt used in the client's verifier.</param>
        /// <param name="identity">The user's identity (eg. username)</param>
        /// <param name="password">The user's password</param>
	    public void GenerateClientCredentials(byte[] salt, byte[] identity, byte[] password)
	    {
	        this.x = Srp6Utilities.CalculateX(digest, N, salt, identity, password);
	    }

        /// <summary>
        /// Generate the secret value
        /// </summary>
        /// <param name="serverB">The server's credentials</param>
        /// <returns>Shared secret value between client and server</returns>
        /// <exception cref="CryptoException">If server's credentials are invalid</exception>
	    public BigInteger CalculateSecret(BigInteger serverB)
	    {
	        this.B = Srp6Utilities.ValidatePublicValue(N, serverB);
	        this.u = Srp6Utilities.CalculateU(digest, N, pubA, B);
	        this.S = CalculateS();

	        return S;
	    }

        /// <summary>
        /// Calculate the client's proof that they know the shared secret
        /// </summary>
        /// <returns>Proof M1</returns>
        public BigInteger CalculateClientProof()
        {
            M1 = Srp6Utilities.CalculateM1(digest, pubA, B, S);
            return M1;
        }

        /// <summary>
        /// Calculate the server's proof that they know the shared secret
        /// </summary>
        /// <returns>Proof M2</returns>
        public BigInteger CalculateServerProof()
        {
            return Srp6Utilities.CalculateM2(digest, pubA, M1, S);
        }

        /// <summary>
        /// Verify the proof of the server that they knew the shared secret
        /// </summary>
        /// <param name="M2">Server proof: M2</param>
        /// <returns>True if valid, false otherwise</returns>
        /// <remarks>
        /// This step is optional in the SRP spec
        /// </remarks>
        public bool VerifyServerProof(BigInteger M2)
        {
            return M2.Equals(CalculateServerProof());
        }

        /// <summary>
        /// Get the hashed secret key to use to encrypt further communication
        /// </summary>
        /// <returns>Hashed Server Key K (=H(S))</returns>
        public byte[] GetKey()
        {
            return Srp6Utilities.HashedKey(digest, S);
        }

	    protected BigInteger SelectPrivateValue()
	    {
	    	return Srp6Utilities.GeneratePrivateValue(digest, N, g, random);    	
	    }

	    private BigInteger CalculateS()
	    {
	        BigInteger k = Srp6Utilities.CalculateK(digest, N, g);
	        BigInteger exp = u.Multiply(x).Add(privA);
	        BigInteger tmp = g.ModPow(x, N).Multiply(k).Mod(N);
	        return B.Subtract(tmp).Mod(N).ModPow(exp, N);
	    }
	}
}
